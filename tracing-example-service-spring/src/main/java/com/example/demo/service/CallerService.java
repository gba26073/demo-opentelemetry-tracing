package com.example.demo.service;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.example.demo.model.Hello;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;

@Service
@Slf4j
public class CallerService {

    @Value("#{environment.DOWNSTREAM_SERVICES_URL}")
    private String[] services;

    @Setter(onMethod = @__({ @Autowired }))
    public WebClient client;

    @Setter(onMethod = @__({ @Autowired }))
    public Random random;

    public Flux<Hello> forward(String variable, Hello hello) {
        // 50% forward request to next service
        var isContinued = random.nextBoolean();
        if (isContinued) {
            var flux = Flux.<Hello>empty();
            for (var service : services) {
                var method = getRandomMethod();
                if (log.isDebugEnabled()) {
                    log.debug("Pass {} to {} with {}", hello, variable, method);
                }
                var call = client.method(method).uri(service + "/" + variable);
                if (hello == null) {
                    hello = new Hello("the method of last request is get or head");
                }
                if (method != HttpMethod.GET && method != HttpMethod.HEAD) {
                    call.contentType(MediaType.APPLICATION_JSON)
                            .accept(MediaType.APPLICATION_JSON)
                            .bodyValue(hello);
                }
                flux = flux.concatWith(call.retrieve()
                        .bodyToFlux(Hello.class));
            }
            return flux;
        }
        // 25% throw error
        var isError = random.nextBoolean();
        if (isError) {
            return Flux.error(new RuntimeException("something error T.T"));
        }
        // 25% stop forwarding request
        return Flux.just(new Hello("It is finished."));
    }

    /**
     * Get http method (except TRACE) randomly.
     * 
     * @return Http method.
     */
    private HttpMethod getRandomMethod() {
        var methods = HttpMethod.values();
        var i = random.nextInt(methods.length - 1);
        return methods[i];
    }

}
