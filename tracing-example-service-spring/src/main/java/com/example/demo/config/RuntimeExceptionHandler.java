package com.example.demo.config;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.example.demo.model.ErrorMessage;

import brave.Tracer;
import lombok.Setter;

@RestControllerAdvice
public class RuntimeExceptionHandler {

    @Setter(onMethod = @__({ @Autowired }))
    public Random random;

    @Setter(onMethod = @__({ @Autowired }))
    private Tracer tracer;

    /**
     * Display trace id in response body.
     * 
     * @param e must not be {@literal null}.
     * @return Http response.
     */
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<?> handle(RuntimeException e) {
        var isServerError = random.nextBoolean();
        var status = isServerError ? HttpStatus.INTERNAL_SERVER_ERROR : HttpStatus.BAD_REQUEST;
        return ResponseEntity.status(status)
                .body(new ErrorMessage(tracer.currentSpan().context().traceIdString(), e.getMessage()));
    }

}
