package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Hello;
import com.example.demo.service.CallerService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Setter;
import reactor.core.publisher.Flux;

@RestController
@Tag(name = "Caller")
public class CallerController {

    @Setter(onMethod = @__({ @Autowired }))
    public CallerService service;

    @Operation(summary = "Forward request with body to another service.")
    @RequestMapping(path = "/{path}", method = {
            RequestMethod.POST, RequestMethod.PUT, RequestMethod.PATCH,
            RequestMethod.DELETE, RequestMethod.OPTIONS
    })
    public Flux<Hello> handleWithBody(@PathVariable String path, @RequestBody(required = false) Hello hello) {
        return service.forward(path, hello);
    }

    @Operation(summary = "Forward request without body to another service.")
    @RequestMapping(path = "/{path}", method = { RequestMethod.GET, RequestMethod.HEAD })
    public Flux<Hello> handleWithoutBody(@PathVariable String path) {
        return service.forward(path, null);
    }

}
