package com.example.demo.config;

import java.util.Random;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class AppConfig {

    @Bean
    public WebClient client() {
        return WebClient.builder().build();
    }

    @Bean
    public Random random() {
        return new Random();
    }

}
